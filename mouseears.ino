#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define HATINCR  230  // time increment between hat updates
#define EARINCR  130  // time increment between ear updates

#define DATAPIN   6   // NeoPixel data pin

#define HATPIXELS 20  // number of pixels around the hat portion
#define EARPIXELS 16  // number of pixels in each ear

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS  (HATPIXELS + 2 * EARPIXELS)

#define HATSTART   0                        // hat pixels begin here
#define EARSTART1  (HATPIXELS)              // ear 1 pixels begin here
#define EARSTART2  (HATPIXELS + EARPIXELS)  // ear 2 pixels begin here

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
static Adafruit_NeoPixel  pixels = Adafruit_NeoPixel(NUMPIXELS, DATAPIN, NEO_GRB + NEO_KHZ800);
static unsigned long      nexthat;    // time for next hat update
static unsigned long      nextear;    // time for next ear update
static byte               hatphase;   // keep track of hat display phase
static byte               earphase;   // keep track of ear display phase

// compute hat color from phase

static uint32_t
mkhatcolor(
  byte phase)   // phase to compute color for
{
  byte  r;      // red value
  byte  g;      // green value
  byte  b;      // blue value
  float theta;  // angle computed from phase

  // each primary color is a different phase sine wave

  theta = (2 * PI * phase) / HATPIXELS;
  r     = 0x7f * (1 + sin(theta + (0 * HATPIXELS) / 3));
  g     = 0x7f * (1 + sin(theta + (1 * HATPIXELS) / 3));
  b     = 0x7f * (1 + sin(theta + (2 * HATPIXELS) / 3));

  return(pixels.Color(r, g, b));
}

// compute ear color from phase

static uint32_t
mkearcolor(
  byte phase)   // phase to compute color for
{
  byte  r;      // red value
  byte  g;      // green value
  byte  b;      // blue value

  // each primary color is a single bit of the phase

  r = (phase & 0x01) ? 0xff : 0x40;
  g = (phase & 0x02) ? 0xff : 0x40;
  b = (phase & 0x04) ? 0xff : 0x40;

  return(pixels.Color(r, g, b));
}

// set hat display based on the phase

static void
sethat(
  byte  phase)
{
  // just turn on a single pixel corresponding to the phase
  pixels.setPixelColor(HATSTART + phase, mkhatcolor(phase));
}

// set ears display based on the phase

static void
setears(
  byte  phase)
{
  uint32_t  pixelcolor;

  pixelcolor = mkearcolor(phase);

  // set one pixel on each ear corresponding to the phase
  // first ear, going forward
  pixels.setPixelColor(EARSTART1 + phase, pixelcolor);
  // second ear, going backward
  pixels.setPixelColor(EARSTART2 + ((EARPIXELS - 1) - phase), pixelcolor);
}

// this gets run once when the sketch is started

void
setup()
{
  pixels.begin(); // This initializes the NeoPixel library.

  // initialize variables

  nexthat  = 0;
  nextear  = 0;
  hatphase = 0;
  earphase = 0;
}

// this gets run over and over for as long as the sketch runs

void
loop()
{
  boolean       change;   // change/update flag
  unsigned long curtime;  // current time

  // initialize current time and change flag
  curtime = millis();
  change  = false;

  // check to see if it's time for a hat update

  if (curtime > nexthat) {
    // time for hat update, update hat phase
    if (++hatphase >= HATPIXELS) {
      // wrap back around to zero
      hatphase = 0;
    }

    nexthat = curtime + HATINCR;  // compute time for next update
    change  = true;               // set update flag
  }

  // check to see if it's time for an ear update
  
  if (curtime > nextear) {
    // time for ear update, update ear phase
    if (++earphase >= EARPIXELS) {
      // wrap back around to zero
      earphase = 0;
    }

    nextear = curtime + EARINCR;  // compute time for next update
    change  = true;               // set update flag
  }

  // see if we need to update the NeoPixels

  if (!change) {
    // no change, nothing more to do
    return;
  }

  // update NeoPixels with current display
  
  pixels.clear();     // turn off all the pixels to begin with
  sethat(hatphase);   // turn on appropriate hat pixels
  setears(earphase);  // turn on appropriate ear pixels
  pixels.show();      // update NeoPixel display
}
